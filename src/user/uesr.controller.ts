import { Controller, Get, Post, Patch, Delete, Param, Body } from '@nestjs/common';
import { UserService } from './user.service';
import { createUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';




@Controller('users')
export class UserController {
    private userService = new UserService;


    @Get()
    find() {
        return this.userService.find();
    }

    @Post("create")
    create(@Body() createUserDto: createUserDto) {
        return this.userService.create(createUserDto);
    }

    @Get(":id")
    findOne(@Param() id) {
        return this.userService.findOne(id);
    }

    @Patch()
    update(@Body() UpdateUserDto: UpdateUserDto) {
        return this.userService.update(UpdateUserDto);
    }

    @Delete()
    delete(@Body() UpdateUserDto: UpdateUserDto) {
        return this.userService.delete(UpdateUserDto);

    }
}
