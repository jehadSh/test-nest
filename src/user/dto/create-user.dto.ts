export class createUserDto {

    readonly id: string;

    readonly email: string;

    readonly name: string;

    readonly phone: string;
}

