import { Injectable } from '@nestjs/common';
import { User, Prisma } from '@prisma/client';

import { PrismaClient } from '@prisma/client'
import { createUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';

const prisma = new PrismaClient();


@Injectable()
export class UserService {

    find() {
        return prisma.user.findMany();
    }

    findOne(id) {
        return prisma.user.findUnique({
            where: {
                id: id
            }
        })
    }

    create(createUserDto: createUserDto) {

        const user = prisma.user.create({
            data: {
                email: createUserDto.email,
                name: createUserDto.name,
                phone: createUserDto.phone
            },
        })
        return user;
    }

    update(UpdateUserDto: UpdateUserDto) {
        const updateUser = prisma.user.update({
            where: {
                email: UpdateUserDto.email,
            },
            data: {
                name: UpdateUserDto.name,
            },
        })

        return updateUser;
    }

    delete(UpdateUserDto: UpdateUserDto) {
        const deleteUser = prisma.user.delete({
            where: {
                email: UpdateUserDto.email,
            },
        })
        return deleteUser;
    }
}